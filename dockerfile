#FROM eclipse-temurin:17-jdk-alpine as builder
#COPY --chown=gradle:gradle . /home/gradle/src
#WORKDIR /home/gradle/src
#RUN gradle build --no-daemon

FROM openjdk:17-jdk-slim
EXPOSE 8080
ARG JAR_FILE=/build/libs/*.jar
COPY /target/*.jar /spring-boot-app.jar
ENTRYPOINT ["java", "-jar", "/spring-boot-app.jar"]
