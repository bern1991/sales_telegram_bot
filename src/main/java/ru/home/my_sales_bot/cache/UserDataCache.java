package ru.home.my_sales_bot.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.Entity.User;
import ru.home.my_sales_bot.enums.BotState;
import ru.home.my_sales_bot.service.abstracts.UserService;

import java.util.HashMap;
import java.util.Map;
@Service
public class UserDataCache implements DataCache {
    private UserService userService;

    @Autowired
    public UserDataCache(UserService userService) {
        this.userService = userService;
    }

    private Map<Long, BotState> userBotStates = new HashMap<>();
    private Map<Long, User> userData = new HashMap<>();

    @Override
    public void setUsersCurrentBotState(Long userId, BotState botState) {
        userBotStates.put(userId, botState);
    }

    @Override
    public BotState getUsersCurrentBotState(Long userId) {
        BotState botState;
        if (!userService.existsUserByTelegramId(userId) && !userBotStates.containsKey(userId)) {
            botState = BotState.WELCOME;
            return botState;
        }
        botState = userBotStates.get(userId);
        if (botState == null) {
            botState = BotState.WAITING;
        }
        return botState;
    }

    @Override
    public User getUserProfile(Long userId) {
        User user = userService.findUserById(userId);
        if (user == null) {
            user = new User();
        }
        return user;
    }

    @Override
    public void saveUserProfile(User user) {
        userService.saveNewUser(user);
    }
}
