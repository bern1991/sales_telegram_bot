package ru.home.my_sales_bot.cache;

import ru.home.my_sales_bot.Entity.User;
import ru.home.my_sales_bot.enums.BotState;

public interface DataCache {
    void setUsersCurrentBotState(Long userId, BotState botState);
    BotState getUsersCurrentBotState(Long userId);
    User getUserProfile(Long userId);
    void saveUserProfile(User user);

}
