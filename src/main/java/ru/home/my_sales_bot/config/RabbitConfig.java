package ru.home.my_sales_bot.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.home.my_sales_bot.config.properties.RabbitProperties;

@Configuration
@RequiredArgsConstructor
public class RabbitConfig {
    private final RabbitProperties rabbitProperties;

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(rabbitProperties.getExchange());
    }
    @Bean
    public Queue myQueue() {
        return new Queue(rabbitProperties.getQueue());
    }

    @Bean
    public Binding bindingExchangeToMyQueue(DirectExchange directExchange, Queue myQueue) {
        return BindingBuilder.bind(myQueue).to(directExchange).with(rabbitProperties.getRoutingKey());

    }
}
