package ru.home.my_sales_bot.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
@ConfigurationProperties(prefix = "rabbit")
public class RabbitProperties {
    private String queue;
    private String exchange;
    private String routingKey;
    private String resultQueue;
}
