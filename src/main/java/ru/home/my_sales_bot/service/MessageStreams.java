package ru.home.my_sales_bot.service;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Producers and Consumers.
 */
public interface MessageStreams {
    String INPUT = "input.goods.save";
    String OUTPUT = "output.goods.save";

    @Input(INPUT)
    SubscribableChannel inboundMessage();

    @Output(OUTPUT)
    MessageChannel outboundMessage();
}
