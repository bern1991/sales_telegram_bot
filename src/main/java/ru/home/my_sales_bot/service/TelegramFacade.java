package ru.home.my_sales_bot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface TelegramFacade {
    SendMessage handleUpdate(Update update);
}
