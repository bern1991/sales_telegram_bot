package ru.home.my_sales_bot.service.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.enums.GoodsStatus;
import ru.home.my_sales_bot.listener.GoodsOutboxMessaging;
import ru.home.my_sales_bot.service.abstracts.GoodsOutboxService;

@Service
@RequiredArgsConstructor
@Slf4j
public class GoodsHandlerSchedulerImpl implements GoodsHandlerScheduler {
    private final GoodsOutboxService goodsOutboxService;
    private final GoodsOutboxMessaging goodsOutboxMessaging;


    @Override
    @Scheduled(fixedDelay = 10000)
    public void process() {
        log.info("Start process goods from DB...");
        goodsOutboxService.findByGoodsStatus(GoodsStatus.NEW)
                .forEach(goodsOutboxMessaging::sendMessage);
    }
}
