package ru.home.my_sales_bot.service.abstracts;

public interface LocaleMessageService {
    String getMessage(String message);
}
