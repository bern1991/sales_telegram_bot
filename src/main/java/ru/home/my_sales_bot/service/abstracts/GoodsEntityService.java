package ru.home.my_sales_bot.service.abstracts;

import ru.home.my_sales_bot.Entity.Goods;
import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;

public interface GoodsEntityService {
    void saveFromOutbox(Goods goods);
    void updateFromOutbox(Goods goods);
    boolean existByName(String name);
}
