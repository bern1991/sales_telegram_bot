package ru.home.my_sales_bot.service.abstracts;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public interface ReplyMessageService {
    SendMessage getReplyMessage(long chatId, String replyMessage);
    SendMessage getReplyMessage(long chatId, String replyMessage, String serviceReply);
}
