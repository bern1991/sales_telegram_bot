package ru.home.my_sales_bot.service.abstracts;

import ru.home.my_sales_bot.Entity.User;

public interface UserService {
    boolean isAdmin(Long id);
    void saveNewUser(User user);
    User findUserById(Long id);
    boolean existsUserByTelegramId(Long id);
    User findUserByTelegramId(Long telegramId);
    void updateUser(User user);
}
