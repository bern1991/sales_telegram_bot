package ru.home.my_sales_bot.service.abstracts;

import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;
import ru.home.my_sales_bot.dto.GoodsStatusDto;
import ru.home.my_sales_bot.enums.GoodsStatus;

import java.util.List;

public interface GoodsOutboxService {
    GoodsHandleMessageDto saveGoodsOutbox(GoodsHandleMessageDto goodsHandleMessageDto);
    GoodsHandleMessageDto updateGoodsOutbox(GoodsHandleMessageDto message);
    List<GoodsHandleMessageDto> findByGoodsStatus(GoodsStatus goodsStatus);
}
