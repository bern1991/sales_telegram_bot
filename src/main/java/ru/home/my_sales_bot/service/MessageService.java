package ru.home.my_sales_bot.service;

import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;
import ru.home.my_sales_bot.dto.GoodsStatusDto;

/**
 * Интерфейс обработки сообщений.
 */
public interface MessageService {
    void sendMessage(GoodsHandleMessageDto messageDto);
    void receiveMessage(GoodsStatusDto messageDto);
}
