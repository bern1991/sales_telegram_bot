package ru.home.my_sales_bot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.Entity.User;
import ru.home.my_sales_bot.repository.UserRepository;
import ru.home.my_sales_bot.service.abstracts.UserService;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean isAdmin(Long id) {
        return userRepository.getUserById(id).isAdmin();
    }

    @Override
    public void saveNewUser(User user) {
        userRepository.save(user);
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.getReferenceById(id);
    }

    @Override
    public boolean existsUserByTelegramId(Long id) {
        return userRepository.existsUserByTelegramId(id);
    }

    @Override
    public User findUserByTelegramId(Long telegramId) {
        return userRepository.getUserByTelegramId(telegramId);
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        userRepository.save(user);
    }
}
