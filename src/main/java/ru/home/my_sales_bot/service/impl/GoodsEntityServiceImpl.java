package ru.home.my_sales_bot.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.Entity.Goods;
import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;
import ru.home.my_sales_bot.mapper.GoodsOutboxMapper;
import ru.home.my_sales_bot.repository.GoodsRepository;
import ru.home.my_sales_bot.service.abstracts.GoodsEntityService;

import javax.transaction.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class GoodsEntityServiceImpl implements GoodsEntityService {
    private final GoodsOutboxMapper goodsOutboxMapper;
    private final GoodsRepository repository;
    @Override
    public void saveFromOutbox(Goods goods) {
        repository.save(goods);
    }

    @Override
    public void updateFromOutbox(Goods goods) {
        Goods goodsInDb = repository.findByName(goods.getName());
        goodsInDb.setCost(goods.getCost());
        repository.save(goodsInDb);
    }

    @Override
    public boolean existByName(String name) {
        return repository.existsByName(name);
    }
}
