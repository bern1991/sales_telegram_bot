package ru.home.my_sales_bot.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.Entity.GoodsOutbox;
import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;
import ru.home.my_sales_bot.enums.GoodsStatus;
import ru.home.my_sales_bot.mapper.GoodsOutboxMapper;
import ru.home.my_sales_bot.repository.GoodsOutboxRepository;
import ru.home.my_sales_bot.service.abstracts.GoodsOutboxService;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class GoodsOutboxServiceImpl implements GoodsOutboxService {
    private final GoodsOutboxMapper goodsOutboxMapper;
    private final GoodsOutboxRepository goodsOutboxRepository;

    public GoodsHandleMessageDto saveGoodsOutbox(GoodsHandleMessageDto goodsHandleMessageDto) {
        return goodsOutboxMapper.toGoodsHandleMessageDto(goodsOutboxRepository.save(
                        goodsOutboxMapper.toGoodsOutbox(goodsHandleMessageDto)
                )
        );
    }

    @Override
    public GoodsHandleMessageDto updateGoodsOutbox(GoodsHandleMessageDto message) {
        GoodsHandleMessageDto goodsHandleMessageDto =
                goodsOutboxRepository.findByGoodsName(message.getGoodsName())
                        .map(goodsOutboxMapper::toGoodsHandleMessageDto)
                        .orElseThrow();
        return goodsOutboxMapper
                .toGoodsHandleMessageDto(
                        goodsOutboxRepository.save(goodsOutboxMapper.toGoodsOutbox(goodsHandleMessageDto))
                );
    }

    public List<GoodsHandleMessageDto> findByGoodsStatus(GoodsStatus goodsStatus) {
        List<GoodsOutbox> listOfEntities = goodsOutboxRepository.findGoodsOutboxByGoodsStatusEquals(goodsStatus);
        return listOfEntities.stream()
                .map(goodsOutboxMapper::toGoodsHandleMessageDto)
                .toList();
    }
}
