package ru.home.my_sales_bot.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.home.my_sales_bot.enums.BotState;
import ru.home.my_sales_bot.botapi.BotStateHandlersFactory;
import ru.home.my_sales_bot.cache.UserDataCache;
import ru.home.my_sales_bot.service.TelegramFacade;

@Service
@Slf4j
public class TelegramFacadeImpl implements TelegramFacade {
    private UserDataCache userDataCache;
    private BotStateHandlersFactory botStateHandlersFactory;

    @Autowired
    public TelegramFacadeImpl(UserDataCache userDataCache, BotStateHandlersFactory botStateHandlersFactory) {
        this.userDataCache = userDataCache;
        this.botStateHandlersFactory = botStateHandlersFactory;
    }

    @Override
    public SendMessage handleUpdate(Update update) {
        SendMessage replyMessage = null;
        Message message = update.getMessage();
        if (update.hasMessage() && update.getMessage().hasText()) {
            log.info("new message from USer:{}, chatId:{}, text:{}",
                    message.getFrom().getUserName(), message.getChatId(), message.getText());
            replyMessage = handleInputMessage(message);
        }
        return replyMessage;
    }

    private SendMessage handleInputMessage(Message message) {
        String inputMessage = message.getText().toLowerCase();
        Long userId = message.getFrom().getId();
        BotState botState;
        SendMessage replyMessage;
        botState = switch (inputMessage) {
            case "/start" -> BotState.WELCOME;
            case "/add_goods" -> BotState.GOODS;
            case "/show_goods" -> BotState.GOODS_SHOW;
            default -> userDataCache.getUsersCurrentBotState(userId);
        };

        userDataCache.setUsersCurrentBotState(userId, botState);
        replyMessage = botStateHandlersFactory.processInputMessage(botState, message);
        return replyMessage;
    }
}

