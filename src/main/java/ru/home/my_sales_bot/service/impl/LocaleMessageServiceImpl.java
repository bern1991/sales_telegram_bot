package ru.home.my_sales_bot.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.service.abstracts.LocaleMessageService;

import java.util.Locale;

@Service
public class LocaleMessageServiceImpl implements LocaleMessageService {
    private final Locale locale;
    private final MessageSource messageSource;

    public LocaleMessageServiceImpl(@Value("${localeTag}")String locale, MessageSource messageSource) {
        this.locale = Locale.forLanguageTag(locale);
        this.messageSource = messageSource;
    }

    @Override
    public String getMessage(String message) {
        return messageSource.getMessage(message, null, locale);
    }
}
