package ru.home.my_sales_bot.service.impl;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.home.my_sales_bot.service.abstracts.LocaleMessageService;
import ru.home.my_sales_bot.service.abstracts.ReplyMessageService;

@Service
public class ReplyMessageServiceImpl implements ReplyMessageService {
    private LocaleMessageService localeMessageService;

    public ReplyMessageServiceImpl(LocaleMessageService localeMessageService) {
        this.localeMessageService = localeMessageService;
    }

    @Override
    public SendMessage getReplyMessage(long chatId, String replyMessage) {
        return new SendMessage(String.valueOf(chatId), localeMessageService.getMessage(replyMessage));
    }

    @Override
    public SendMessage getReplyMessage(long chatId, String replyMessage, String serviceReply) {
        return new SendMessage(String.valueOf(chatId), String.format(localeMessageService.getMessage(replyMessage),
                serviceReply));
    }
}
