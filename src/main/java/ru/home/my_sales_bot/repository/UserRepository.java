package ru.home.my_sales_bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.home.my_sales_bot.Entity.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User getUserById(Long id);
    User getUserByTelegramId(Long telegramId);
    boolean existsUserByTelegramId(Long telegramId);
}
