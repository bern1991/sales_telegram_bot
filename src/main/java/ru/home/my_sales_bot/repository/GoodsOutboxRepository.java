package ru.home.my_sales_bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.home.my_sales_bot.Entity.GoodsOutbox;
import ru.home.my_sales_bot.enums.GoodsStatus;

import java.util.List;
import java.util.Optional;

public interface GoodsOutboxRepository extends JpaRepository<GoodsOutbox, Long> {
    List<GoodsOutbox> findGoodsOutboxByGoodsStatusEquals(GoodsStatus goodsStatus);

    Optional<GoodsOutbox> findByGoodsName(String name);
}
