package ru.home.my_sales_bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.home.my_sales_bot.Entity.Goods;

@Repository
public interface GoodsRepository extends JpaRepository<Goods, Long> {
    boolean existsByName(String name);
    Goods findByName(String name);
}
