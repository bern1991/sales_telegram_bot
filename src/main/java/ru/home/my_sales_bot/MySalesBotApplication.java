package ru.home.my_sales_bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@EnableScheduling
@EnableAspectJAutoProxy
public class MySalesBotApplication {
    public static void main(String[] args) {
        SpringApplication.run(MySalesBotApplication.class);
    }

    private void test() {
        Map<List<String>, String> map = new HashMap<>();
        List<String> list = new ArrayList<>();

        map.put(list, "hello");
        list.add("world");

        System.out.println(map.remove(list));
    }
}
