package ru.home.my_sales_bot.enums;

public enum GoodsStatus {
    NEW,
    PROCESSING,
    PROCESSED,
    ERROR
}
