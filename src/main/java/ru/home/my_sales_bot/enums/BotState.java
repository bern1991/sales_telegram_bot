package ru.home.my_sales_bot.enums;

public enum BotState {
    WELCOME,
    WELCOME_ASKINGFORKEY,

    GOODS,
    GOODS_START,
    GOODS_ADD,
    GOODS_ADDNAME,
    GOODS_SHOW,

    SHOW_MENU,

    ADD_URL,
    UPDATE_GOODS,
    LIST_USERS,

    WAITING
}
