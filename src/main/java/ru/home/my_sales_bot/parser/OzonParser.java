package ru.home.my_sales_bot.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.home.my_sales_bot.Entity.Goods;
@Component
@Slf4j
public class OzonParser implements Parser {
    public static final String OZON_RU = "ozon.ru";

    @Override
    public Goods parse(String url) {
        //TODO реализация парсера Озон
        return null;
    }

    @Override
    public boolean handle(String url) {
        return url.contains(OZON_RU);
    }
}
