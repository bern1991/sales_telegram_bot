package ru.home.my_sales_bot.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.home.my_sales_bot.Entity.Goods;

@Component
@Slf4j
public class WbParser implements Parser {

    public static final String WWW_WILDBERRIES_RU = "www.wildberries.ru";

    @Override
    public Goods parse(String url) {
        return new Goods();
    }

    @Override
    public boolean handle(String url) {
        return url.contains(WWW_WILDBERRIES_RU);
    }
}
