package ru.home.my_sales_bot.parser;

import ru.home.my_sales_bot.Entity.Goods;
import ru.home.my_sales_bot.Entity.GoodsOutbox;

public interface Parser {
    Goods parse(String url);
    boolean handle(String url);
}
