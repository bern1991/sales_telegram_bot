package ru.home.my_sales_bot.dto;

import lombok.Data;
import ru.home.my_sales_bot.enums.GoodsStatus;

@Data
public class GoodsStatusDto {
    private String name;
    private GoodsStatus goodsStatus;
}
