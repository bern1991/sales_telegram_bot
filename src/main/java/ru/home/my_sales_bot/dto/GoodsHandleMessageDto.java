package ru.home.my_sales_bot.dto;

import lombok.Data;
import ru.home.my_sales_bot.enums.GoodsStatus;

@Data
public class GoodsHandleMessageDto {
    private Long userId;
    private String goodsName;
    private String url;
    private GoodsStatus goodsStatus;

    public String infoMessage() {
        return "\n Название товара: " + goodsName
                + "Ссылка на товар: " + url;
    }

}
