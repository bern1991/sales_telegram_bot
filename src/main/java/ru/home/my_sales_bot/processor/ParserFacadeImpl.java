package ru.home.my_sales_bot.processor;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.Entity.Goods;
import ru.home.my_sales_bot.config.properties.RabbitProperties;
import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;
import ru.home.my_sales_bot.enums.GoodsStatus;
import ru.home.my_sales_bot.mapper.GoodsOutboxMapper;
import ru.home.my_sales_bot.parser.Parser;
import ru.home.my_sales_bot.repository.GoodsRepository;
import ru.home.my_sales_bot.service.abstracts.GoodsEntityService;

import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class ParserFacadeImpl implements ParserFacade {
    private final GoodsEntityService goodsEntityService;
    private final Set<Parser> parsers;
    private final RabbitProperties properties;
    private final RabbitTemplate rabbitTemplate;
    private final GoodsOutboxMapper goodsOutboxMapper;

    public void process(GoodsHandleMessageDto goodsHandleMessageDto) {
        if (goodsEntityService.existByName(goodsHandleMessageDto.getGoodsName())) {
            goodsHandleMessageDto.setGoodsStatus(GoodsStatus.PROCESSED);
            rabbitTemplate.convertAndSend(properties.getResultQueue(), goodsHandleMessageDto);
            return;
        }
        String url = goodsHandleMessageDto.getUrl();
        Parser parser = parsers.stream()
                .filter(p -> p.handle(goodsHandleMessageDto.getUrl()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Parser not found for incoming URL: " + url));
        Goods goods = parser.parse(url);
        goods.setStatus(GoodsStatus.PROCESSED);
        goodsEntityService.saveFromOutbox(goods);
        rabbitTemplate.convertAndSend(properties.getResultQueue(),
                goodsOutboxMapper.toGoodsHandleMessageDtoFromGoodsEntity(goods));
    }
}
