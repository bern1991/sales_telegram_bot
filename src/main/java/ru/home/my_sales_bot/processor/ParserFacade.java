package ru.home.my_sales_bot.processor;

import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;

public interface ParserFacade {
    void process(GoodsHandleMessageDto goodsHandleMessageDto);
}
