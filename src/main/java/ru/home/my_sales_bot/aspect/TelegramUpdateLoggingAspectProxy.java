package ru.home.my_sales_bot.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Aspect
@Component
@Slf4j
public class TelegramUpdateLoggingAspectProxy {

    @Before("execution(* ru.home.my_sales_bot.service.impl.TelegramFacadeImpl.handleUpdate(..)) && args(update)")
    public void logUpdate(JoinPoint joinPoint, Update update) {
        log.info("[TELEGRAM-LONG-POLLING-I001] Received update: {}", update);
    }
}