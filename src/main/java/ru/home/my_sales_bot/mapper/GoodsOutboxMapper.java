package ru.home.my_sales_bot.mapper;

import org.mapstruct.Mapper;
import ru.home.my_sales_bot.Entity.Goods;
import ru.home.my_sales_bot.Entity.GoodsOutbox;
import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;

@Mapper(componentModel = "spring")
public interface GoodsOutboxMapper {
    GoodsHandleMessageDto toGoodsHandleMessageDto(GoodsOutbox goodsOutbox);
    GoodsOutbox toGoodsOutbox(GoodsHandleMessageDto goodsHandleMessageDto);
    Goods toGoodsEntityFromGoodsHandleMessageDto(GoodsHandleMessageDto goodsHandleMessageDto);
    GoodsHandleMessageDto toGoodsHandleMessageDtoFromGoodsEntity(Goods Goods);
}
