package ru.home.my_sales_bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
@ConditionalOnProperty("${webhook.enabled}")
public class WebHookController {
    private AtomicInteger count;
    private final TelegramWebhookBot tgbot;

    @Autowired
    public WebHookController(TelegramWebhookBot tgbot) {
        this.tgbot = tgbot;
    }
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public BotApiMethod<?> onUpdateReceived(@RequestBody Update update){
        count.getAndIncrement();
        return tgbot.onWebhookUpdateReceived(update);
    }
}
