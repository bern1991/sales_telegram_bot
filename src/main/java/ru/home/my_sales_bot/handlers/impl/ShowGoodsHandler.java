package ru.home.my_sales_bot.handlers.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.home.my_sales_bot.Entity.User;
import ru.home.my_sales_bot.cache.UserDataCache;
import ru.home.my_sales_bot.enums.BotState;
import ru.home.my_sales_bot.handlers.abstracts.InputMessageHandler;
import ru.home.my_sales_bot.repository.GoodsOutboxRepository;
import ru.home.my_sales_bot.service.abstracts.ReplyMessageService;
import ru.home.my_sales_bot.service.abstracts.UserService;

@Service
@RequiredArgsConstructor
@Slf4j
public class ShowGoodsHandler implements InputMessageHandler {
    private final UserDataCache userDataCache;
    private final ReplyMessageService messageService;
    private final UserService userServiceImpl;
    private final GoodsOutboxRepository goodsOutboxRepository;

    @Override
    public SendMessage handle(Message message) {
        if (userDataCache.getUsersCurrentBotState(message.getFrom().getId()).equals(BotState.GOODS_SHOW)) {
            userDataCache.setUsersCurrentBotState(message.getFrom().getId(), BotState.GOODS_SHOW);
        }
        return processUsersInput(message);    }

    @Override
    public BotState getHandlerName() {
        return BotState.GOODS_SHOW;
    }

    private SendMessage processUsersInput(Message inputMsg) {
        SendMessage replyToUser = new SendMessage();
        Long userId = inputMsg.getFrom().getId();
        User user = userServiceImpl.findUserByTelegramId(userId);
        if (user == null) {
            return replyToUser = messageService.getReplyMessage(inputMsg.getChatId(), "reply.query.failed");
        }
        System.out.println(userDataCache.getUsersCurrentBotState(user.getTelegramId()));
        BotState botState = userDataCache.getUsersCurrentBotState(user.getTelegramId());

        if (botState.equals(BotState.GOODS_SHOW)) {
            String example = "This is an example from goods repository answer";
            replyToUser = messageService.getReplyMessage(inputMsg.getChatId(), "reply.goods.show", example);
            userDataCache.setUsersCurrentBotState(user.getTelegramId(), BotState.WAITING);
        }

        return replyToUser;
    }
}
