package ru.home.my_sales_bot.handlers.impl;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.home.my_sales_bot.enums.BotState;
import ru.home.my_sales_bot.cache.UserDataCache;
import ru.home.my_sales_bot.handlers.abstracts.InputMessageHandler;
import ru.home.my_sales_bot.service.abstracts.ReplyMessageService;

@Component
public class WaitingHandler implements InputMessageHandler {
    private UserDataCache userDataCache;
    private ReplyMessageService replyMessageService;

    public WaitingHandler(UserDataCache userDataCache, ReplyMessageService replyMessageService) {
        this.userDataCache = userDataCache;
        this.replyMessageService = replyMessageService;
    }

    @Override
    public SendMessage handle(Message message) {
        if (userDataCache.getUsersCurrentBotState(message.getFrom().getId()).equals(BotState.WAITING)) {
            userDataCache.setUsersCurrentBotState(message.getFrom().getId(), BotState.WAITING);
        }
        return processUsersInput(message);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.WAITING;
    }
    private SendMessage processUsersInput(Message inputMsg) {
        Long userId = inputMsg.getFrom().getId();
        long chatId = inputMsg.getChatId();
        SendMessage replyToUser = new SendMessage();

        BotState botState = userDataCache.getUsersCurrentBotState(userId);
        if (botState.equals(BotState.WAITING)) {
            replyToUser = replyMessageService.getReplyMessage(chatId, "reply.waiting.info");
            userDataCache.setUsersCurrentBotState(userId, BotState.WAITING);
        }
        return replyToUser;
    }
}
