package ru.home.my_sales_bot.handlers.impl;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.home.my_sales_bot.Entity.User;
import ru.home.my_sales_bot.enums.BotState;
import ru.home.my_sales_bot.cache.UserDataCache;
import ru.home.my_sales_bot.handlers.abstracts.InputMessageHandler;
import ru.home.my_sales_bot.service.abstracts.ReplyMessageService;
import ru.home.my_sales_bot.service.abstracts.UserService;

@Component
public class AuthorizationHandler implements InputMessageHandler {
    private UserDataCache userDataCache;
    ReplyMessageService messageService;
    private UserService userService;

    public AuthorizationHandler(UserDataCache userDataCache, ReplyMessageService messageService, UserService userService) {
        this.userDataCache = userDataCache;
        this.messageService = messageService;
        this.userService = userService;
    }

    @Override
    public SendMessage handle(Message message) {
        if (userDataCache.getUsersCurrentBotState(message.getFrom().getId()).equals(BotState.WELCOME)) {
            userDataCache.setUsersCurrentBotState(message.getFrom().getId(), BotState.WELCOME);
        }
        return processUsersInput(message);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.WELCOME;
    }

    private SendMessage processUsersInput(Message inputMsg) {
        Long userId = inputMsg.getFrom().getId();
        long chatId = inputMsg.getChatId();
        SendMessage replyToUser = new SendMessage();
        System.out.println(userDataCache.getUsersCurrentBotState(userId));
        BotState botState = userDataCache.getUsersCurrentBotState(userId);

        if(botState.equals(BotState.WELCOME)) {
            if (userService.existsUserByTelegramId(userId)) {
                replyToUser = messageService.getReplyMessage(chatId, String.format("reply.auth.user_existed", inputMsg.getFrom().getUserName()));
                userDataCache.setUsersCurrentBotState(userId, BotState.WAITING);
            } else {
                replyToUser = messageService.getReplyMessage(chatId, "reply.auth.askingForKey");
                userDataCache.setUsersCurrentBotState(userId, BotState.WELCOME_ASKINGFORKEY);
            }
        }

        if(botState.equals(BotState.WELCOME_ASKINGFORKEY)) {
            if (inputMsg.getText().toLowerCase().contains("system99-")) {
                User user = new User();
                user.setTelegramId(inputMsg.getFrom().getId());
                user.setFirstName(inputMsg.getFrom().getFirstName());
                user.setLastName(inputMsg.getFrom().getLastName());
                user.setAdmin(true);
                userService.saveNewUser(user);
                replyToUser = messageService.getReplyMessage(chatId, "reply.auth.rightKey");

            } else {
                replyToUser = messageService.getReplyMessage(chatId, "reply.auth.wrongKey");

            }
            userDataCache.setUsersCurrentBotState(userId, BotState.WAITING);
        }
        return replyToUser;
    }
}
