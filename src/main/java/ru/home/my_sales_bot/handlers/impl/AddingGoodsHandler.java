package ru.home.my_sales_bot.handlers.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.home.my_sales_bot.Entity.User;
import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;
import ru.home.my_sales_bot.enums.BotState;
import ru.home.my_sales_bot.cache.UserDataCache;
import ru.home.my_sales_bot.enums.GoodsStatus;
import ru.home.my_sales_bot.handlers.abstracts.InputMessageHandler;
import ru.home.my_sales_bot.mapper.GoodsOutboxMapper;
import ru.home.my_sales_bot.repository.GoodsOutboxRepository;
import ru.home.my_sales_bot.service.abstracts.ReplyMessageService;
import ru.home.my_sales_bot.service.abstracts.UserService;

import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class AddingGoodsHandler implements InputMessageHandler {
    private final UserDataCache userDataCache;
    private final ReplyMessageService messageService;
    private final UserService userService;
    private final GoodsOutboxRepository goodsOutboxRepository;
    private final GoodsOutboxMapper goodsOutboxMapper;
    private Map<Long, GoodsHandleMessageDto> savingGoodsCache = new HashMap<>(100);


    @Override
    public SendMessage handle(Message message) {
        if (userDataCache.getUsersCurrentBotState(message.getFrom().getId()).equals(BotState.GOODS)) {
            userDataCache.setUsersCurrentBotState(message.getFrom().getId(), BotState.GOODS);
        }
        return processUsersInput(message);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.GOODS;
    }

    private SendMessage processUsersInput(Message inputMsg) {
        SendMessage replyToUser = new SendMessage();
        Long userId = inputMsg.getFrom().getId();
        User user = userService.findUserByTelegramId(userId);
        if (user == null) {
            return replyToUser = messageService.getReplyMessage(inputMsg.getChatId(), "reply.query.failed");
        }
        BotState botState = userDataCache.getUsersCurrentBotState(user.getTelegramId());

        if (botState.equals(BotState.GOODS)) {
            replyToUser = messageService.getReplyMessage(inputMsg.getChatId(), "reply.goods.add");
            userDataCache.setUsersCurrentBotState(user.getTelegramId(), BotState.GOODS_ADD);
        }

        if (botState.equals(BotState.GOODS_ADD)) {
            GoodsHandleMessageDto goods = new GoodsHandleMessageDto();
            goods.setUrl(inputMsg.getText());
            goods.setUserId(userId);
            savingGoodsCache.put(userId, goods);
            replyToUser = messageService.getReplyMessage(inputMsg.getChatId(), "reply.goods.addName");
            userDataCache.setUsersCurrentBotState(user.getTelegramId(), BotState.GOODS_ADDNAME);
        }
        if (botState.equals(BotState.GOODS_ADDNAME)) {
            GoodsHandleMessageDto goods = savingGoodsCache.get(userId);
            goods.setGoodsName(inputMsg.getText());
            goods.setGoodsStatus(GoodsStatus.NEW);
            goodsOutboxRepository.save(goodsOutboxMapper.toGoodsOutbox(goods));
            savingGoodsCache.clear();
            replyToUser = messageService.getReplyMessage(inputMsg.getChatId(), "reply.goods.added", goods.infoMessage() );
        }

        return replyToUser;
    }
}
