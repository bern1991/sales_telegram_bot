package ru.home.my_sales_bot.Entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.home.my_sales_bot.enums.GoodsStatus;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Table(name = "goods_outbox")
public class GoodsOutbox {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String goodsName;
    private String url;
    private Long userId;
    private GoodsStatus goodsStatus;
}
