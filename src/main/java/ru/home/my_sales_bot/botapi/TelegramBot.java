package ru.home.my_sales_bot.botapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.home.my_sales_bot.config.properties.BotProperties;
import ru.home.my_sales_bot.service.impl.TelegramFacadeImpl;

@Component
@ConditionalOnProperty("${!bot.webhook.enabled}")
public class TelegramBot extends TelegramWebhookBot {

    private TelegramFacadeImpl telegramFacadeImpl;
    private BotProperties botProperties;

    @Autowired
    public TelegramBot(BotProperties botProperties, TelegramFacadeImpl telegramFacadeImpl) {
        this.botProperties = botProperties;
        this.telegramFacadeImpl = telegramFacadeImpl;
    }

    @Override
    public String getBotUsername() {
        return botProperties.getName();
    }

    @Override
    public String getBotToken() {
        return botProperties.getToken();
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            return telegramFacadeImpl.handleUpdate(update);
        }
        return null;
    }

    @Override
    public String getBotPath() {
        return botProperties.getWebHook();
    }

}
