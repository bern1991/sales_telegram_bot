package ru.home.my_sales_bot.botapi;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.home.my_sales_bot.config.properties.BotProperties;
import ru.home.my_sales_bot.service.impl.TelegramFacadeImpl;

@Component
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "webhook", name = "enabled", havingValue = "false")
public class TelegramLongpollingBot extends TelegramLongPollingBot {
    private final TelegramFacadeImpl telegramFacadeImpl;
    private final BotProperties botProperties;

    @Override
    public String getBotUsername() {
        return botProperties.getName();
    }

    @Override
    public String getBotToken() {
        return botProperties.getToken();
    }
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            try {
                execute(telegramFacadeImpl.handleUpdate(update));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
