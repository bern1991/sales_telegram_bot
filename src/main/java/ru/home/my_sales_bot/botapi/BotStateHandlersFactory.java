package ru.home.my_sales_bot.botapi;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.home.my_sales_bot.enums.BotState;
import ru.home.my_sales_bot.handlers.abstracts.InputMessageHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BotStateHandlersFactory {
    private Map<BotState, InputMessageHandler> messageHandlers = new HashMap<>();

    public BotStateHandlersFactory(List<InputMessageHandler> messageHandlers) {
        messageHandlers.forEach(handler -> this.messageHandlers.put(handler.getHandlerName(), handler));
    }

    public SendMessage processInputMessage(BotState currentSate, Message message) {
        InputMessageHandler currentMessageHandler = getMessageHandler(currentSate);
        return currentMessageHandler.handle(message);
    }

    private InputMessageHandler getMessageHandler(BotState currentState) {
        if (isAuthorizationState(currentState)) {
            return messageHandlers.get(BotState.WELCOME);
        }
        if (isWaitingState(currentState)) {
            return messageHandlers.get(BotState.WAITING);
        }
        if(isWorkWithGoodsState(currentState)) {
            return messageHandlers.get(BotState.GOODS);
        }

        return messageHandlers.get(currentState);
    }

    private boolean isWaitingState(BotState currentState) {
        return switch (currentState) {
            case WAITING -> true;
            default -> false;
        };
    }

    private boolean isAuthorizationState(BotState currentState) {
        return switch (currentState) {
            case WELCOME, WELCOME_ASKINGFORKEY -> true;
            default -> false;
        };
    }

    private boolean isWorkWithGoodsState(BotState currentState) {
        return switch (currentState) {
            case GOODS, GOODS_ADD, GOODS_ADDNAME -> true;
            default -> false;
        };
    }


}
