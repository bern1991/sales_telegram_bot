package ru.home.my_sales_bot.listener;

import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;

public interface GoodsOutboxMessaging {
    void sendMessage(GoodsHandleMessageDto message) ;
    public void receiveMessage(String message);
}
