package ru.home.my_sales_bot.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ru.home.my_sales_bot.config.properties.RabbitProperties;
import ru.home.my_sales_bot.dto.GoodsHandleMessageDto;
import ru.home.my_sales_bot.processor.ParserFacade;
import ru.home.my_sales_bot.listener.GoodsOutboxMessaging;

@RequiredArgsConstructor
@Service
@Slf4j
public class GoodsOutboxMessagingImpl implements GoodsOutboxMessaging {
    private final RabbitProperties rabbitProperties;
    private final RabbitTemplate rabbitTemplate;
    private final ParserFacade parserFacade;
    private final ObjectMapper objectMapper;

    @Override
    public void sendMessage(GoodsHandleMessageDto messageDto) {
        try {
            rabbitTemplate.convertAndSend(rabbitProperties.getQueue(), objectMapper.writeValueAsString(messageDto));
        } catch (Exception e) {
            log.error("Failed to send message: ",e);
        }
    }

    @Override
    @RabbitListener(queues = "${rabbit.queue}")
    public void receiveMessage(String message) {
        try {
            parserFacade.process(objectMapper.readValue(message, GoodsHandleMessageDto.class));
        } catch (Exception e) {
            log.error("Failed to send message: ",e);
        }
    }
}
